package net.molchat.jping.ui.impl;

import net.molchat.jping.io.JpingMessage;
import net.molchat.jping.ui.UiManager;
import net.molchat.jping.ui.WindowsManager;
import net.molchat.jping.ui.tray.TrayManager;
import net.molchat.jping.ui.tray.impl.DefaultTrayManager;

/**
 * Created 26.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class DefaultUiManager implements UiManager {

    private TrayManager trayManager;
    private WindowsManager winManager;

    @Override
    public void startUI() {

        trayManager = new DefaultTrayManager();
        trayManager.showTrayIcon();

        winManager = new DefaultWindowsManager();
    }

    @Override
    public void stopUI() {

        trayManager.removeIcons();
    }

    @Override
    public void handleIncomingMessage(JpingMessage jpingMessage) {


        winManager.showPingedDialog(jpingMessage);
    }
}
