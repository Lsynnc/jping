package net.molchat.jping.ui.impl;

import net.molchat.jping.io.JpingMessage;
import net.molchat.jping.ui.WindowsManager;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

/**
 * Created 29.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class DefaultWindowsManager implements WindowsManager {

    protected PingWindowsAdapter pingWindowsAdapter = new PingWindowsAdapter();


    @Override
    public void showPingedDialog(JpingMessage jpingMessage) {

        showOnEachScreen(jpingMessage);
    }

    private void showOnEachScreen(JpingMessage jpingMessage) {

        GraphicsEnvironment ge = GraphicsEnvironment.
                getLocalGraphicsEnvironment();
        GraphicsDevice[] gs = ge.getScreenDevices();
        for (int j = 0; j < gs.length; j++) {
            GraphicsDevice gd = gs[j];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            for (int i = 0; i < gc.length; i++) {
                JFrame jFrame = createPingFrame(gc[i], jpingMessage);
                pingWindowsAdapter.addFrame(jFrame);
                jFrame.setVisible(true);
            }
        }

        pingWindowsAdapter.displayAll();
    }

    private JFrame createPingFrame(GraphicsConfiguration config, JpingMessage jpingMessage) {

        JFrame jFrame = new JFrame(config);
        jFrame.setTitle("Ping");

        try {
            Image image = ImageIO.read(this.getClass().getResourceAsStream("/res/ui/tray/bullsIcon.png"));
            jFrame.setIconImage(image);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Label emptyLabel = new Label("Click to close");

        jFrame.getContentPane().add(emptyLabel, BorderLayout.CENTER);
        jFrame.pack();

        jFrame.addWindowListener(pingWindowsAdapter);
        emptyLabel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                pingWindowsAdapter.windowClosing(null);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

        Rectangle monitorBounds = config.getBounds();
        int frameW = (int) monitorBounds.getWidth() >> 2;
        int frameH = (int) monitorBounds.getHeight() >> 2;
        int frameX = (int) (monitorBounds.getWidth() / 2 - frameW / 2 + monitorBounds.getX());
        int frameY = (int) (monitorBounds.getHeight() / 2 - frameH / 2 + monitorBounds.getY());

        Rectangle frameBounds = new Rectangle();
        frameBounds.setBounds(frameX, frameY, frameW, frameH);

        jFrame.setBounds(frameBounds);

        return jFrame;
    }


    public static void showOnScreen(int screenNumber, JFrame frame) {
        GraphicsEnvironment ge = GraphicsEnvironment
                .getLocalGraphicsEnvironment();
        GraphicsDevice[] gs = ge.getScreenDevices();
        if (screenNumber > -1 && screenNumber < gs.length) {
            gs[screenNumber].setFullScreenWindow(frame);
        }
    }


}
