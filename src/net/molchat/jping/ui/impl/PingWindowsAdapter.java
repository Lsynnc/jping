package net.molchat.jping.ui.impl;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Created 29.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class PingWindowsAdapter extends WindowAdapter {

    protected List<JFrame> pingWindows = new ArrayList<>();


    public void addFrame(JFrame jFrame) {

        pingWindows.add(jFrame);
    }

    // Display ping windows
    public void displayAll() {
        for (JFrame jFrame : pingWindows) {
            jFrame.setVisible(true);
        }
    }

    private void closeAllPingWindows() {
        for (JFrame jFrame : pingWindows){
            jFrame.dispose();
        }

        pingWindows.clear();
    }

    @Override
    public void windowClosing(WindowEvent e){

        closeAllPingWindows();
    }
}
