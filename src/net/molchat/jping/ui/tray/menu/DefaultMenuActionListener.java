package net.molchat.jping.ui.tray.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

/**
 * Created 28.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class DefaultMenuActionListener implements ActionListener {

    private static final Logger LOG = Logger.getLogger(DefaultMenuActionListener.class.getName());

    @Override
    public void actionPerformed(ActionEvent e) {

        LOG.info("Action performed: " + e.toString());
    }
}
