package net.molchat.jping.ui.tray.menu.actions;

import net.molchat.jping.commands.GlobalPingCommang;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created 28.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class PingAllPopupMenuActionListener implements ActionListener {

    private static final Logger LOG = Logger.getLogger(PingAllPopupMenuActionListener.class.getName());

    @Override
    public void actionPerformed(ActionEvent event) {

        GlobalPingCommang pingCommand = new GlobalPingCommang();
        try {
            pingCommand.perform();
        } catch (IOException e) {
            LOG.log(Level.WARNING, "Can't perform Global Ping command", e);
        }
    }
}
