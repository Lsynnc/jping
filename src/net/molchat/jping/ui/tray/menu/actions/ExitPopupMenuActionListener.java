package net.molchat.jping.ui.tray.menu.actions;

import net.molchat.jping.commands.GlobalExitCommand;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created 28.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class ExitPopupMenuActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {

        GlobalExitCommand exitCommand = new GlobalExitCommand();
        exitCommand.perform();
    }
}
