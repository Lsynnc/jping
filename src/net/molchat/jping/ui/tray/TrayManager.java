package net.molchat.jping.ui.tray;

/**
 * Created 26.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public interface TrayManager {

    public void showTrayIcon();

    public void updateTrayIcon();

    /**
     * Removes all program icons shown in system tray. Useful for global exit.
     *
     */
    public void removeIcons();
}
