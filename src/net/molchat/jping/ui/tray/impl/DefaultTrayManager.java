package net.molchat.jping.ui.tray.impl;

import net.molchat.jping.ui.tray.TrayManager;
import net.molchat.jping.ui.tray.menu.actions.ExitPopupMenuActionListener;
import net.molchat.jping.ui.tray.menu.actions.PingAllPopupMenuActionListener;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created 26.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class DefaultTrayManager implements TrayManager {

    private static final Logger LOG = Logger.getLogger(DefaultTrayManager.class.getName());

    protected TrayIcon trayIcon;


    public TrayIcon getTrayIcon() {
        return trayIcon;
    }

    public void setTrayIcon(TrayIcon trayIcon) {
        this.trayIcon = trayIcon;
    }

    @Override
    public void showTrayIcon() {

        if (SystemTray.isSupported()) {

            try {
                SystemTray tray = SystemTray.getSystemTray();

                Image image = ImageIO.read(this.getClass().getResourceAsStream("/res/ui/tray/bullsIcon.png"));

                PopupMenu popup = createMenu();


                // construct a TrayIcon
                trayIcon = new TrayIcon(image, "JPing", popup);
                // set the TrayIcon properties
                // trayIcon.addActionListener(actionListener);

                // add the tray image
                try {
                    tray.add(trayIcon);
                } catch (AWTException e) {
                    System.err.println(e);
                }
            } catch (Exception e) {
                LOG.log(Level.FINE, "Can't create tray icon ", e);
            }
        } else {
            throw new RuntimeException("Tray icons are not supported");
        }
    }

    protected PopupMenu createMenu() {

        // create a popup menu
        PopupMenu popup = new PopupMenu();

        addMenuGlobalCommands(popup);
        return popup;
    }

    protected void addMenuGlobalCommands(PopupMenu popup) {

        popup.add(createMenuItem("Ping All", new PingAllPopupMenuActionListener()));
        popup.addSeparator();
        // Add accessible users here
//        popup.add(createMenuItem("Settings", null));
//        popup.addSeparator();
        popup.add(createMenuItem("Exit", new ExitPopupMenuActionListener()));
    }

    protected MenuItem createMenuItem(String name, ActionListener actionListener) {

        MenuItem menuItem = new MenuItem(name);
        menuItem.addActionListener(actionListener);
        return menuItem;
    }

    @Override
    public void updateTrayIcon() {
        // ...
        // some time later
        // the application state has changed - update the image
        if (trayIcon != null) {
//            trayIcon.setImage(updatedImage);
        }
        // ...
    }

    @Override
    public void removeIcons() {

        if (trayIcon != null) {
            SystemTray tray = SystemTray.getSystemTray();
            tray.remove(trayIcon);
        }
    }
}
