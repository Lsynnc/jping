package net.molchat.jping.ui;

import net.molchat.jping.io.JpingMessage;

/**
 * Created 26.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public interface UiManager {

    public void startUI();

    public void stopUI();

    public void handleIncomingMessage(JpingMessage jpingMessage);
}
