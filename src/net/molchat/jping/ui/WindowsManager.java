package net.molchat.jping.ui;

import net.molchat.jping.io.JpingMessage;

/**
 * Created 29.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public interface WindowsManager {

    public void showPingedDialog(JpingMessage jpingMessage);
}
