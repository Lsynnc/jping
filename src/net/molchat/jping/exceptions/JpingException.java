package net.molchat.jping.exceptions;

/**
 * Created 20.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class JpingException extends RuntimeException {

    public JpingException(String message) {

        super(message);
    }
}
