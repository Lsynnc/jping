package net.molchat.jping.commands;

import net.molchat.jping.client.JpingClient;
import net.molchat.jping.main.Registry;
import net.molchat.jping.ui.UiManager;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Shuts down JPing.
 * <p/>
 * Created 28.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class GlobalExitCommand {

    private static final Logger LOG = Logger.getLogger(GlobalExitCommand.class.getName());

    public void perform() {

        stopPresentThread();
        stopUI();
        System.exit(0);
    }

    private void stopUI() {

        final Registry registry = Registry.getInstance();

        try {
            final UiManager uiManager = (UiManager) registry.getObject(Registry.Keys.UI_MANAGER);
            uiManager.stopUI();
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Error while trying to stop user interface", e);
        }
    }

    private void stopPresentThread() {

        final Registry registry = Registry.getInstance();

        try {
            final JpingClient jpingClient = (JpingClient) registry.getObject(Registry.Keys.JPING_CLIENT);
            jpingClient.stop();
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Error while trying to stop client thread", e);
        }
    }


}
