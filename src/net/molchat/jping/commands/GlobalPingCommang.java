package net.molchat.jping.commands;

import net.molchat.jping.client.JpingClient;
import net.molchat.jping.main.Registry;

import java.io.IOException;

/**
 * Sends a global ping message. Should display a ping window for everyone, who's now available in the network.
 * <p/>
 * Created 28.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class GlobalPingCommang {

    public void perform() throws IOException {

        final Registry registry = Registry.getInstance();
        final JpingClient jpingClient = (JpingClient) registry.getObject(Registry.Keys.JPING_CLIENT);

        jpingClient.sendGlobalPing();
    }
}
