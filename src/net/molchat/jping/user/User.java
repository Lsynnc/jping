package net.molchat.jping.user;

import net.molchat.binroot.Root;
import net.molchat.binroot.Rootable;

import java.util.Properties;
import java.util.UUID;

/**
 * POJO representating a User object
 * <p/>
 * Created 20.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class User implements Rootable {

    public static final int ROOT_NODE_ID = 3;

    private static final int NAME_ID = 1;
    private static final int LAST_NAME_ID = 2;
    private static final int UUID_ID = 3;

    protected String name;
    protected String lastName;
    protected UUID uuid;


    @Override
    public Root toRoot() {

        Root user = new Root(User.ROOT_NODE_ID);
        user.setData(uuid.toString());
        user.addString(NAME_ID, name);
        user.addString(LAST_NAME_ID, lastName);

        return user;
    }

    @Override
    public void fromRoot(Root root) {

        uuid = UUID.fromString(root.asString());
        name = root.getChild(NAME_ID).asString();
    }

    public void selfConfigure(Properties properties) {

        final String uuidStr = properties.getProperty("user.uuid");
        if (uuidStr != null && uuidStr.length() > 0) {
            uuid = UUID.fromString(uuidStr);
        } else {
            uuid = UUID.randomUUID();
        }

        name = properties.getProperty("user.name.first");
        if (name == null || name.length() <= 0) {
            name = uuid.toString();
        }

        lastName = properties.getProperty("user.name.last");
    }
}
