package net.molchat.jping.client;

import java.io.IOException;
import java.net.SocketException;

/**
 * Created 20.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public interface JpingClient {

    public void startPresentThread(int pauseIntervalMs, int port);

    public void stop();

    public void sendGlobalPing() throws IOException;
}
