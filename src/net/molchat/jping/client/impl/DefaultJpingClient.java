package net.molchat.jping.client.impl;

import net.molchat.binroot.Root;
import net.molchat.binroot.RootWriter;
import net.molchat.jping.client.JpingClient;
import net.molchat.jping.io.JpingMessage;
import net.molchat.jping.io.impl.JpingPingMessage;
import net.molchat.jping.main.Registry;
import net.molchat.jping.user.User;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created 20.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class DefaultJpingClient implements JpingClient, Runnable {

    private final static Logger LOG = Logger.getLogger(DefaultJpingClient.class.getName());

    private int pauseIntervalMs;
    private int port;
    private Thread thisThread;
    private boolean isRunning = false;


    @Override
    public void startPresentThread(int pauseIntervalMs, int port) {

        this.pauseIntervalMs = pauseIntervalMs;
        this.port = port;

        thisThread = new Thread(this);
        thisThread.setDaemon(true);
        isRunning = true;
        thisThread.start();
    }

    @Override
    public void stop() {

        isRunning = false;
        thisThread.interrupt();
    }

    @Override
    public void sendGlobalPing() throws IOException {

        LOG.info("Sending a global ping");
        JpingMessage jpingMessage = new JpingPingMessage();
        sendBroadcastPacket(jpingMessage);
    }

    private void sendBroadcastPacket(JpingMessage jpingMessage) throws IOException {

        final Root jpingMessageRoot = jpingMessage.toRoot();
        sendBroadcastPacket(jpingMessageRoot);
    }


    private void sendBroadcastPacket(Root root) throws IOException {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        RootWriter rootWriter = new RootWriter(byteArrayOutputStream);
        rootWriter.writeRoot(root);
        final byte[] sendData = byteArrayOutputStream.toByteArray();
        sendBroadcastPacket(sendData);
    }


    private void sendBroadcastPacket(byte[] sendData) throws IOException {

        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress ipAddress = InetAddress.getByAddress(new byte[]{-1, -1, -1, -1});
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ipAddress, port);
        clientSocket.send(sendPacket);
        clientSocket.close();
    }

    @Override
    public void run() {

        while (isRunning) {
            try {

                User user = (User) Registry.getInstance().getObject(Registry.Keys.USER);
                if (user == null) {
                    throw new RuntimeException("User object is not found");
                }

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                RootWriter rootWriter = new RootWriter(byteArrayOutputStream);
                rootWriter.writeRoot(user.toRoot());
                final byte[] sendData = byteArrayOutputStream.toByteArray();
                sendBroadcastPacket(sendData);
                LOG.info("Sent:" + user.toRoot().asString());

                thisThread.sleep(pauseIntervalMs);
            } catch (InterruptedException ie) {
                LOG.log(Level.INFO, "Client thread was interupted");
            } catch (IOException e) {
                LOG.log(Level.FINE, "Exception during output", e);
            } catch (Exception e) {
                LOG.log(Level.FINE, "Unknown exception during output", e);
            }
        }
    }
}
