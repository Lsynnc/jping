package net.molchat.jping.main;

import java.util.HashMap;
import java.util.Map;

/**
 * Singleton
 * <p/>
 * Created 28.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class Registry {

    private Map<String, Object> registryMap;

    private static Registry instance;

    public static interface Keys {

        public static final String USER = "user";
        public static final String UI_MANAGER = "uiManager";
        public static final String JPING_CLIENT = "jpingClient";
        public static final String JPING_SERVER = "jpingServer";
    }

    private Registry() {

        registryMap = new HashMap<>();
    }

    public static synchronized Registry getInstance() {

        if (instance == null) {
            instance = new Registry();
        }

        return instance;
    }

    public Object getObject(String key) {

        return getObject(key, null);
    }

    public Object getObject(String key, Object defaultObject) {

        if (key != null) {
            final Object object = registryMap.get(key);
            if (object != null) {
                return object;
            } else {
                return defaultObject;
            }
        } else {
            return defaultObject;
        }
    }

    public void putObject(String key, Object object) {

        registryMap.put(key, object);
    }
}
