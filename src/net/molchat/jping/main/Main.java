package net.molchat.jping.main;

import net.molchat.jping.client.JpingClient;
import net.molchat.jping.client.impl.DefaultJpingClient;
import net.molchat.jping.config.Config;
import net.molchat.jping.server.JpingServer;
import net.molchat.jping.server.impl.DefaultJpingServer;
import net.molchat.jping.ui.impl.DefaultUiManager;
import net.molchat.jping.ui.UiManager;
import net.molchat.jping.user.User;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main {


    // "User is present" message interval, millis
    private static final int PRESENT_THREAD_INTERVAL = 60 * 1000;

    protected static Logger LOG;


    public static void main(String[] args) throws IOException {

        configLogger();
        config();
        startUi();
        startServer();
    }

    protected static void configLogger() {
        try {
            final InputStream resourceAsStream = Main.class.getResourceAsStream("/res/logging.properties");
            LogManager.getLogManager().readConfiguration(resourceAsStream);
        } catch (IOException e) {
            System.err.println("Could not setup logger configuration: " + e.toString());
        }

        LOG = Logger.getLogger(Main.class.getName());

        LOG.log(Level.FINE, "Port number is: " + Config.getProperty(Config.PROP_SERVER_PORT));
    }

    private static void startUi() {

        UiManager uiManager = new DefaultUiManager();
        uiManager.startUI();
        Registry.getInstance().putObject(Registry.Keys.UI_MANAGER, uiManager);
    }


    private static void config() {

        Config.readDefaultProperties();
        User user = new User();
        user.selfConfigure(Config.getProperties());
        Registry.getInstance().putObject(Registry.Keys.USER, user);
    }

    private static void startServer() throws IOException {

        final Registry registry = Registry.getInstance();

        JpingServer jpingServer = new DefaultJpingServer();
        final int port = Integer.parseInt(Config.getProperty(Config.PROP_SERVER_PORT));
        jpingServer.setPort(port);
        jpingServer.start();
        registry.putObject(Registry.Keys.JPING_SERVER, jpingServer);

        JpingClient jpingClient = new DefaultJpingClient();
        jpingClient.startPresentThread(PRESENT_THREAD_INTERVAL, port);
        registry.putObject(Registry.Keys.JPING_CLIENT, jpingClient);
    }


    private static void setConsoleLevel(Level level) {

        //get the top Logger:
        Logger topLogger = Logger.getLogger("");

        // Handler for console (reuse it if it already exists)
        Handler consoleHandler = null;
        //see if there is already a console handler
        for (Handler handler : topLogger.getHandlers()) {
            if (handler instanceof ConsoleHandler) {
                //found the console handler
                consoleHandler = handler;
                break;
            }
        }


        if (consoleHandler != null) {
            //set the console handler to fine:
            consoleHandler.setLevel(level);
        }
//        else {
//            //there was no console handler found, create a new one
//            consoleHandler = new ConsoleHandler();
//            topLogger.addHandler(consoleHandler);
//            //set the console handler to fine:
//            consoleHandler.setLevel(java.util.logging.Level.FINEST);
//        }
    }
}
