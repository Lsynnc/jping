package net.molchat.jping.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Valentyn on 13.07.2014.
 */
public class Config {


    private final static String FULL_FILE_NAME = "/res/config.properties";
    private final static Logger LOG = Logger.getLogger(Config.class.getName());

    public static final String PROP_SERVER_PORT = "jping.server.port";
    public static final String PROP_LOG_LEVEL = "jping.log.level";

    private static final String USER_PROPERTIES_FILE_NAME = "user.properties";
    private static final String CONFIG_FOLDER_NAME = "config";
    private static final String PROGRAM_FOLDER_NAME = "JPing";

    private static Properties stringProperties = new Properties();
    private static boolean isInitialized = false;

    public static String getProperty(String key) {

        return getProperty(key, null);
    }


    public static String getProperty(String key, String defaultValue) {

        if (!isInitialized) {
            readDefaultProperties();
            isInitialized = true;
        }

        return stringProperties.getProperty(key, defaultValue);
    }


    public static void readDefaultProperties() {

        readPropertiesFromJar(FULL_FILE_NAME);

        String userHome = System.getProperty("user.home");

        if (userHome != null) {
            String configFolderPath = userHome + File.separator + PROGRAM_FOLDER_NAME + File.separator + CONFIG_FOLDER_NAME;
            readPropertiesFromFile(configFolderPath + File.separator + USER_PROPERTIES_FILE_NAME);
        }
    }

    private static void readPropertiesFromFile(String fullFileName) {

        try {
            //load the file handle for main.properties
            FileInputStream fileInputStream = new FileInputStream(fullFileName);
            if (fileInputStream != null) {
                stringProperties.load(fileInputStream);
                fileInputStream.close();
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public static void readPropertiesFromJar(String fullFileName) {

        try {
            InputStream stream = Config.class.getResourceAsStream(fullFileName);
            if (stream != null) {
                stringProperties.load(stream);
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public static Properties getProperties() {
        return stringProperties;
    }

}
