package net.molchat.jping.io;

/**
 * Created 20.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public interface NetworkListener {

    public void setPort(int portNumber);

    public void start();

    public void stop();

    public void setDataReader(DataReader streamReader);
}
