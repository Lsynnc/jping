package net.molchat.jping.io;

import net.molchat.jping.io.impl.ReachText;

/**
 * Created 20.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public interface JpingTextMessage extends JpingMessage {

    public void setText(ReachText text);

    public ReachText getText();
}
