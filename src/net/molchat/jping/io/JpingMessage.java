package net.molchat.jping.io;

import net.molchat.binroot.Root;
import net.molchat.binroot.Rootable;
import net.molchat.jping.user.User;

import java.util.List;

/**
 * Created 20.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public interface JpingMessage extends Rootable {

    public int getType();

    public List<User> getTargetUsers();

    public void setTargetUsers(List<User> users);

    public User getSender();

    public void setSender(User sender);

    public Root getContent();

    public void setContent(Root content);
}
