package net.molchat.jping.io.impl;

import net.molchat.jping.io.DataReader;
import net.molchat.jping.io.NetworkListener;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Created 20.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class DefaultNetworkListener implements NetworkListener, Runnable {

    private int port;
    private DataReader dataReader;
    private Thread currentThread;
    private boolean fRunned;


    @Override
    public void setPort(int portNumber) {

        this.port = portNumber;
    }

    @Override
    public void start() {

        fRunned = true;
        currentThread = new Thread(this);
        currentThread.start();
    }

    @Override
    public void stop() {

        currentThread.interrupt();
    }

    @Override
    public void setDataReader(DataReader dataReader) {

        this.dataReader = dataReader;
    }

    @Override
    public void run() {

        try {
            DatagramSocket serverSocket = new DatagramSocket(port);
            byte[] receiveData = new byte[1024];
            while (fRunned) {

                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                serverSocket.receive(receivePacket);

                final DataPacket dataPacket = new DataPacket();
                dataPacket.bytes = receivePacket.getData();
                dataPacket.inetAddress = receivePacket.getAddress();

                dataReader.receivedData(dataPacket);
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        fRunned = false;
    }
}
