package net.molchat.jping.io.impl;

import net.molchat.binroot.Root;
import net.molchat.jping.io.JpingMessage;
import net.molchat.jping.user.User;

import java.util.List;

/**
 * Created 28.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class JpingPingMessage implements JpingMessage {

    public static final int ROOT_NODE_ID = 1;

    protected List<User> targetUsers;
    protected User sender;
    protected Root content;

    @Override
    public int getType() {

        return ROOT_NODE_ID;
    }

    @Override
    public List<User> getTargetUsers() {

        return targetUsers;
    }

    @Override
    public void setTargetUsers(List<User> users) {

        this.targetUsers = users;
    }

    @Override
    public User getSender() {

        return sender;
    }

    @Override
    public void setSender(User sender) {

        this.sender = sender;
    }

    @Override
    public Root getContent() {

        return content;
    }

    @Override
    public void setContent(Root content) {

        this.content = content;
    }

    @Override
    public Root toRoot() {

        Root pingMess = new Root();
        pingMess.nodeId = ROOT_NODE_ID;

        return pingMess;
    }

    @Override
    public void fromRoot(Root root) {

        // TODO implement method
    }
}
