package net.molchat.jping.io.impl;

import java.net.InetAddress;

/**
 * Created 20.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class DataPacket {

    byte[] bytes;
    InetAddress inetAddress;
}
