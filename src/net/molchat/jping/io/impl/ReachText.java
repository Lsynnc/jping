package net.molchat.jping.io.impl;

import net.molchat.binroot.Root;
import net.molchat.binroot.Rootable;
import net.molchat.jping.exceptions.JpingException;

/**
 * Created 20.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class ReachText implements Rootable {

    private String plainText;
    private Root reachText;

    public String getPlainText() {
        return plainText;
    }

    public void setPlainText(String plainText) {
        this.plainText = plainText;
    }

    public Root getReachText() {

        throw new JpingException("Reach text does not supported in current version");
//        return reachText;
    }

    public void setReachText(Root reachText) {
        this.reachText = reachText;
    }


    @Override
    public Root toRoot() {

        // TODO implement method
        return null;
    }

    @Override
    public void fromRoot(Root root) {

        // TODO implement method

    }
}
