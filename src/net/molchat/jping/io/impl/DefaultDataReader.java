package net.molchat.jping.io.impl;

import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import net.molchat.binroot.Root;
import net.molchat.binroot.RootReader;
import net.molchat.jping.io.DataReader;
import net.molchat.jping.io.handlers.InHandler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created 20.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class DefaultDataReader implements DataReader {

    private static final Logger LOG = Logger.getLogger(DefaultDataReader.class.getName());

    protected Map<Integer, InHandler> inHandlers = new HashMap<>();


    @Override
    public void receivedData(DataPacket dataPacket) {

        final byte[] bytes = dataPacket.bytes;
        LOG.info("Received data: " + (new String(bytes)));

        if (bytes != null && bytes.length > 0) {
            ByteInputStream byteInputStream = new ByteInputStream(bytes, 0, bytes.length);
            RootReader rootReader = new RootReader(byteInputStream);
            try {
                final Root root = rootReader.readRoot();
                LOG.info("Received data as Root: \n" + root.toString());

                final InHandler inHandler = inHandlers.get(root.nodeId);
                if (inHandler != null) {
                    inHandler.handleIncomingMessage(root);
                } else {
                    LOG.info("We have no hadlers registered for message type №" + root.nodeId);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void registerHandler(int messageType, InHandler inHandler) {

        inHandlers.put(messageType, inHandler);
    }
}
