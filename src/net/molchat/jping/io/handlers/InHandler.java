package net.molchat.jping.io.handlers;

import net.molchat.binroot.Root;

/**
 * Created 20.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public interface InHandler {

    public void handleIncomingMessage(Root message);
}
