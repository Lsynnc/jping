package net.molchat.jping.io.handlers.impl;

import net.molchat.binroot.Root;
import net.molchat.jping.io.JpingMessage;
import net.molchat.jping.io.handlers.InHandler;
import net.molchat.jping.io.impl.JpingPingMessage;
import net.molchat.jping.main.Registry;
import net.molchat.jping.ui.UiManager;

import java.util.logging.Logger;

/**
 * Created 29.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class PingHandler implements InHandler {

    private static final Logger LOG = Logger.getLogger(PingHandler.class.getName());

    @Override
    public void handleIncomingMessage(Root message) {

        LOG.info("PingHandler got a message: " + message);

        JpingMessage jpingMessage = new JpingPingMessage();
        jpingMessage.fromRoot(message);

        final UiManager uiManager = (UiManager) Registry.getInstance().getObject(Registry.Keys.UI_MANAGER);
        uiManager.handleIncomingMessage(jpingMessage);
    }
}
