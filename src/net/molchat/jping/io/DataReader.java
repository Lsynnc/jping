package net.molchat.jping.io;

import net.molchat.jping.io.handlers.InHandler;
import net.molchat.jping.io.impl.DataPacket;

/**
 * Created 20.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public interface DataReader {

    public void receivedData(DataPacket dataPacket);

    public void registerHandler(int messageType, InHandler inHandler);
}
