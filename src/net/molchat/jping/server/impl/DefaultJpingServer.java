package net.molchat.jping.server.impl;

import net.molchat.jping.io.DataReader;
import net.molchat.jping.io.NetworkListener;
import net.molchat.jping.io.handlers.impl.PingHandler;
import net.molchat.jping.io.impl.DefaultDataReader;
import net.molchat.jping.io.impl.DefaultNetworkListener;
import net.molchat.jping.io.impl.JpingPingMessage;
import net.molchat.jping.server.JpingServer;

import java.io.IOException;

/**
 * JPing server. Listens to the given port and notifies if got something interesting.
 * <p/>
 * <p/>
 * Created 20.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class DefaultJpingServer implements JpingServer {


    private int port;

    private NetworkListener networkListener;


    @Override
    public void setPort(int portNumber) {

        this.port = portNumber;
    }

    @Override
    public void setProperty(String key, String value) {

        // TODO implement method

    }

    @Override
    public int start() throws IOException {

        networkListener = new DefaultNetworkListener();
        networkListener.setPort(port);

        final DataReader dataReader = new DefaultDataReader();
        // Setup handlers for incoming messages
        dataReader.registerHandler(JpingPingMessage.ROOT_NODE_ID, new PingHandler());

        networkListener.setDataReader(dataReader);
        networkListener.start();

        return port;
    }


    @Override
    public void stop() {

        this.networkListener.stop();
    }
}
