package net.molchat.jping.server;

import java.io.IOException;

/**
 * Created 20.01.2015.
 *
 * @author Valentyn Markovych, Gorilla
 */
public interface JpingServer {

    public void setPort(int portNumber);

    public void setProperty(String key, String value);

    public int start() throws IOException;

    public void stop();
}
